const gulp = require('gulp');
const babel = require('gulp-babel');

const jsSrcPath = 'src/js/**/*.js';

gulp.task('babel', () =>
    gulp.src(jsSrcPath)
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(gulp.dest('dist'))
);

gulp.task('babel:watch', function () {
    gulp.watch(jsSrcPath, gulp.series('babel'));
});